msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2011-06-11 08:32+0300\n"
"Last-Translator: Tommi Vainikainen <tvainika@debian.org>\n"
"Language-Team: Finnish\n"
"Language: Finnish\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/countries.wml:111
msgid "United Arab Emirates"
msgstr "Arabiemiirikunnat"

#: ../../english/template/debian/countries.wml:114
msgid "Albania"
msgstr "Albania"

#: ../../english/template/debian/countries.wml:117
msgid "Armenia"
msgstr "Armenia"

#: ../../english/template/debian/countries.wml:120
msgid "Argentina"
msgstr "Argentiina"

#: ../../english/template/debian/countries.wml:123
msgid "Austria"
msgstr "Itävalta"

#: ../../english/template/debian/countries.wml:126
msgid "Australia"
msgstr "Australia"

#: ../../english/template/debian/countries.wml:129
msgid "Bosnia and Herzegovina"
msgstr "Bosnia ja Hertsegovina"

#: ../../english/template/debian/countries.wml:132
msgid "Bangladesh"
msgstr "Bangladesh"

#: ../../english/template/debian/countries.wml:135
msgid "Belgium"
msgstr "Belgia"

#: ../../english/template/debian/countries.wml:138
msgid "Bulgaria"
msgstr "Bulgaria"

#: ../../english/template/debian/countries.wml:141
msgid "Brazil"
msgstr "Brasilia"

#: ../../english/template/debian/countries.wml:144
msgid "Bahamas"
msgstr "Bahama"

#: ../../english/template/debian/countries.wml:147
msgid "Belarus"
msgstr "Valkovenäjä"

#: ../../english/template/debian/countries.wml:150
msgid "Canada"
msgstr "Kanada"

#: ../../english/template/debian/countries.wml:153
msgid "Switzerland"
msgstr "Sveitsi"

#: ../../english/template/debian/countries.wml:156
msgid "Chile"
msgstr "Chile"

#: ../../english/template/debian/countries.wml:159
msgid "China"
msgstr "Kiina"

#: ../../english/template/debian/countries.wml:162
msgid "Colombia"
msgstr "Kolumbia"

#: ../../english/template/debian/countries.wml:165
msgid "Costa Rica"
msgstr "Costa Rica"

#: ../../english/template/debian/countries.wml:168
msgid "Czech Republic"
msgstr "Tsekki"

#: ../../english/template/debian/countries.wml:171
msgid "Germany"
msgstr "Saksa"

#: ../../english/template/debian/countries.wml:174
msgid "Denmark"
msgstr "Tanska"

#: ../../english/template/debian/countries.wml:177
msgid "Dominican Republic"
msgstr "Dominikaaninen tasavalta"

#: ../../english/template/debian/countries.wml:180
msgid "Algeria"
msgstr "Algeria"

#: ../../english/template/debian/countries.wml:183
msgid "Ecuador"
msgstr "Ecuador"

#: ../../english/template/debian/countries.wml:186
msgid "Estonia"
msgstr "Viro"

#: ../../english/template/debian/countries.wml:189
msgid "Egypt"
msgstr "Egypti"

#: ../../english/template/debian/countries.wml:192
msgid "Spain"
msgstr "Espanja"

#: ../../english/template/debian/countries.wml:195
msgid "Ethiopia"
msgstr "Etiopia"

#: ../../english/template/debian/countries.wml:198
msgid "Finland"
msgstr "Suomi"

#: ../../english/template/debian/countries.wml:201
msgid "Faroe Islands"
msgstr "Färsaaret"

#: ../../english/template/debian/countries.wml:204
msgid "France"
msgstr "Ranska"

#: ../../english/template/debian/countries.wml:207
msgid "United Kingdom"
msgstr "Iso-Britannia"

#: ../../english/template/debian/countries.wml:210
msgid "Grenada"
msgstr "Grenada"

#: ../../english/template/debian/countries.wml:213
msgid "Georgia"
msgstr "Georgia"

#: ../../english/template/debian/countries.wml:216
msgid "Greenland"
msgstr "Grönlanti"

#: ../../english/template/debian/countries.wml:219
msgid "Greece"
msgstr "Kreikka"

#: ../../english/template/debian/countries.wml:222
msgid "Guatemala"
msgstr "Guatemala"

#: ../../english/template/debian/countries.wml:225
msgid "Hong Kong"
msgstr "Hongkong"

#: ../../english/template/debian/countries.wml:228
msgid "Honduras"
msgstr "Honduras"

#: ../../english/template/debian/countries.wml:231
msgid "Croatia"
msgstr "Kroatia"

#: ../../english/template/debian/countries.wml:234
msgid "Hungary"
msgstr "Unkari"

#: ../../english/template/debian/countries.wml:237
msgid "Indonesia"
msgstr "Indonesia"

#: ../../english/template/debian/countries.wml:240
msgid "Iran"
msgstr "Iran"

#: ../../english/template/debian/countries.wml:243
msgid "Ireland"
msgstr "Irlanti"

#: ../../english/template/debian/countries.wml:246
msgid "Israel"
msgstr "Israel"

#: ../../english/template/debian/countries.wml:249
msgid "India"
msgstr "Intia"

#: ../../english/template/debian/countries.wml:252
msgid "Iceland"
msgstr "Islanti"

#: ../../english/template/debian/countries.wml:255
msgid "Italy"
msgstr "Italia"

#: ../../english/template/debian/countries.wml:258
msgid "Jordan"
msgstr "Jordania"

#: ../../english/template/debian/countries.wml:261
msgid "Japan"
msgstr "Japani"

#: ../../english/template/debian/countries.wml:264
msgid "Kenya"
msgstr "Kenia"

#: ../../english/template/debian/countries.wml:267
msgid "Korea"
msgstr "Korea"

#: ../../english/template/debian/countries.wml:270
msgid "Kuwait"
msgstr "Kuwait"

#: ../../english/template/debian/countries.wml:273
msgid "Kazakhstan"
msgstr "Kazakstan"

#: ../../english/template/debian/countries.wml:276
msgid "Sri Lanka"
msgstr "Sri Lanka"

#: ../../english/template/debian/countries.wml:279
msgid "Lithuania"
msgstr "Liettua"

#: ../../english/template/debian/countries.wml:282
msgid "Luxembourg"
msgstr "Luxemburg"

#: ../../english/template/debian/countries.wml:285
msgid "Latvia"
msgstr "Latvia"

#: ../../english/template/debian/countries.wml:288
msgid "Morocco"
msgstr "Marokko"

#: ../../english/template/debian/countries.wml:291
msgid "Moldova"
msgstr "Moldova"

#: ../../english/template/debian/countries.wml:294
msgid "Montenegro"
msgstr "Montenegro"

#: ../../english/template/debian/countries.wml:297
msgid "Madagascar"
msgstr "Madagaskar"

#: ../../english/template/debian/countries.wml:300
msgid "Macedonia, Republic of"
msgstr "Makedonian tasavalta"

#: ../../english/template/debian/countries.wml:303
msgid "Mongolia"
msgstr "Mongolia"

#: ../../english/template/debian/countries.wml:306
msgid "Malta"
msgstr "Malta"

#: ../../english/template/debian/countries.wml:309
msgid "Mexico"
msgstr "Meksiko"

#: ../../english/template/debian/countries.wml:312
msgid "Malaysia"
msgstr "Malesia"

#: ../../english/template/debian/countries.wml:315
msgid "New Caledonia"
msgstr "Uusi-Kaledonia"

#: ../../english/template/debian/countries.wml:318
msgid "Nicaragua"
msgstr "Nicaragua"

#: ../../english/template/debian/countries.wml:321
msgid "Netherlands"
msgstr "Alankomaat"

#: ../../english/template/debian/countries.wml:324
msgid "Norway"
msgstr "Norja"

#: ../../english/template/debian/countries.wml:327
msgid "New Zealand"
msgstr "Uusi-Seelanti"

#: ../../english/template/debian/countries.wml:330
msgid "Panama"
msgstr "Panama"

#: ../../english/template/debian/countries.wml:333
msgid "Peru"
msgstr "Peru"

#: ../../english/template/debian/countries.wml:336
msgid "French Polynesia"
msgstr "Ranskan Polynesia"

#: ../../english/template/debian/countries.wml:339
msgid "Philippines"
msgstr "Filippiinit"

#: ../../english/template/debian/countries.wml:342
msgid "Pakistan"
msgstr "Pakistan"

#: ../../english/template/debian/countries.wml:345
msgid "Poland"
msgstr "Puola"

#: ../../english/template/debian/countries.wml:348
msgid "Portugal"
msgstr "Portugali"

#: ../../english/template/debian/countries.wml:351
msgid "Réunion"
msgstr ""

#: ../../english/template/debian/countries.wml:354
msgid "Romania"
msgstr "Romania"

#: ../../english/template/debian/countries.wml:357
msgid "Serbia"
msgstr "Serbia"

#: ../../english/template/debian/countries.wml:360
msgid "Russia"
msgstr "Venäjä"

#: ../../english/template/debian/countries.wml:363
msgid "Saudi Arabia"
msgstr "Saudi-Arabia"

#: ../../english/template/debian/countries.wml:366
msgid "Sweden"
msgstr "Ruotsi"

#: ../../english/template/debian/countries.wml:369
msgid "Singapore"
msgstr "Singapore"

#: ../../english/template/debian/countries.wml:372
msgid "Slovenia"
msgstr "Slovenia"

#: ../../english/template/debian/countries.wml:375
msgid "Slovakia"
msgstr "Slovakia"

#: ../../english/template/debian/countries.wml:378
msgid "El Salvador"
msgstr "El Salvador"

#: ../../english/template/debian/countries.wml:381
msgid "Thailand"
msgstr "Thaimaa"

#: ../../english/template/debian/countries.wml:384
msgid "Tajikistan"
msgstr "Tadžikistan"

#: ../../english/template/debian/countries.wml:387
msgid "Tunisia"
msgstr "Tunisia"

#: ../../english/template/debian/countries.wml:390
msgid "Turkey"
msgstr "Turkki"

#: ../../english/template/debian/countries.wml:393
msgid "Taiwan"
msgstr "Taiwan"

#: ../../english/template/debian/countries.wml:396
msgid "Ukraine"
msgstr "Ukraina"

#: ../../english/template/debian/countries.wml:399
msgid "United States"
msgstr "Yhdysvallat"

#: ../../english/template/debian/countries.wml:402
msgid "Uruguay"
msgstr "Uruguay"

#: ../../english/template/debian/countries.wml:405
msgid "Uzbekistan"
msgstr "Uzbekistan"

#: ../../english/template/debian/countries.wml:408
msgid "Venezuela"
msgstr "Venezuela"

#: ../../english/template/debian/countries.wml:411
msgid "Vietnam"
msgstr "Vietnam"

#: ../../english/template/debian/countries.wml:414
msgid "Vanuatu"
msgstr "Vanuatu"

#: ../../english/template/debian/countries.wml:417
msgid "South Africa"
msgstr "Etelä-Afrikka"

#: ../../english/template/debian/countries.wml:420
msgid "Zimbabwe"
msgstr "Zimbabwe"

#~ msgid "Great Britain"
#~ msgstr "Iso-Britannia"

#~ msgid "Yugoslavia"
#~ msgstr "Jugoslavia"
