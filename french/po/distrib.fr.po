# Translation of distrib web pages to French
# Copyright (C) 2004, 2005, 2007-2010 Debian French l10n team <debian-l10n-french@lists.debian.org>
# This file is distributed under the same license as the Debian web site.
#
# Frederic Bothamy, 2004, 2005, 2007, 2008.
# Thomas Huriaux, 2005.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2008, 2009.
# David Prévot <david@tilapin.org>, 2010.
# Baptiste Jammet <baptiste@mailoo.org>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: debian webwml distrib\n"
"PO-Revision-Date: 2017-05-11 10:23+0200\n"
"Last-Translator: Baptiste Jammet <baptiste@mailoo.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"

#: ../../english/distrib/search_contents-form.inc:9
#: ../../english/distrib/search_packages-form.inc:8
msgid "Keyword"
msgstr "Mot-clé "

#: ../../english/distrib/search_contents-form.inc:13
msgid "Display"
msgstr "Afficher "

#: ../../english/distrib/search_contents-form.inc:16
msgid "paths ending with the keyword"
msgstr "les chemins se terminant par le mot-clé"

#: ../../english/distrib/search_contents-form.inc:19
msgid "packages that contain files named like this"
msgstr "les paquets contenant un fichier de ce nom"

#: ../../english/distrib/search_contents-form.inc:22
msgid "packages that contain files whose names contain the keyword"
msgstr "les paquets contenant un fichier dont le nom contient le mot-clé"

#: ../../english/distrib/search_contents-form.inc:25
#: ../../english/distrib/search_packages-form.inc:25
msgid "Distribution"
msgstr "Distribution "

#: ../../english/distrib/search_contents-form.inc:27
#: ../../english/distrib/search_packages-form.inc:27
msgid "experimental"
msgstr "experimental"

#: ../../english/distrib/search_contents-form.inc:28
#: ../../english/distrib/search_packages-form.inc:28
msgid "unstable"
msgstr "unstable"

#: ../../english/distrib/search_contents-form.inc:29
#: ../../english/distrib/search_packages-form.inc:29
msgid "testing"
msgstr "testing"

#: ../../english/distrib/search_contents-form.inc:30
#: ../../english/distrib/search_packages-form.inc:30
msgid "stable"
msgstr "stable"

#: ../../english/distrib/search_contents-form.inc:31
#: ../../english/distrib/search_packages-form.inc:31
msgid "oldstable"
msgstr "ancienne stable"

#: ../../english/distrib/search_contents-form.inc:33
msgid "Architecture"
msgstr "Architecture "

#: ../../english/distrib/search_contents-form.inc:38
#: ../../english/distrib/search_packages-form.inc:32
#: ../../english/distrib/search_packages-form.inc:39
msgid "any"
msgstr "toutes"

#: ../../english/distrib/search_contents-form.inc:48
#: ../../english/distrib/search_packages-form.inc:43
msgid "Search"
msgstr "Rechercher"

#: ../../english/distrib/search_contents-form.inc:49
#: ../../english/distrib/search_packages-form.inc:44
msgid "Reset"
msgstr "Réinitialiser"

#: ../../english/distrib/search_packages-form.inc:12
msgid "Search on"
msgstr "Rechercher sur "

#: ../../english/distrib/search_packages-form.inc:14
msgid "Package names only"
msgstr "Noms de paquets seulement"

#: ../../english/distrib/search_packages-form.inc:16
msgid "Descriptions"
msgstr "Descriptions"

#: ../../english/distrib/search_packages-form.inc:18
msgid "Source package names"
msgstr "Noms de paquets-sources"

#: ../../english/distrib/search_packages-form.inc:21
msgid "Only show exact matches"
msgstr "N'afficher que les correspondances exactes"

#: ../../english/distrib/search_packages-form.inc:34
msgid "Section"
msgstr "Section "

#: ../../english/distrib/search_packages-form.inc:36
msgid "main"
msgstr "main"

#: ../../english/distrib/search_packages-form.inc:37
msgid "contrib"
msgstr "contrib"

#: ../../english/distrib/search_packages-form.inc:38
msgid "non-free"
msgstr "non-free"

#: ../../english/releases/arches.data:8
msgid "Alpha"
msgstr "Alpha"

#: ../../english/releases/arches.data:9
msgid "64-bit PC (amd64)"
msgstr "PC 64 bits (amd64)"

#: ../../english/releases/arches.data:10
msgid "ARM"
msgstr "ARM"

#: ../../english/releases/arches.data:11
msgid "64-bit ARM (AArch64)"
msgstr "ARM 64 bits (AArch64)"

#: ../../english/releases/arches.data:12
msgid "EABI ARM (armel)"
msgstr "EABI ARM (armel)"

#: ../../english/releases/arches.data:13
msgid "Hard Float ABI ARM (armhf)"
msgstr "ARM avec unité de calcul flottant (armhf)"

#: ../../english/releases/arches.data:14
msgid "HP PA-RISC"
msgstr "HP PA-RISC"

#: ../../english/releases/arches.data:15
msgid "Hurd 32-bit PC (i386)"
msgstr "Hurd PC 32 bits (i386)"

#: ../../english/releases/arches.data:16
msgid "32-bit PC (i386)"
msgstr "PC 32 bits (i386)"

#: ../../english/releases/arches.data:17
msgid "Intel Itanium IA-64"
msgstr "Intel Itanium IA-64"

#: ../../english/releases/arches.data:18
msgid "kFreeBSD 32-bit PC (i386)"
msgstr "kFreeBSD PC 32 bits (i386)"

#: ../../english/releases/arches.data:19
msgid "kFreeBSD 64-bit PC (amd64)"
msgstr "kFreeBSD PC 64 bits (amd64)"

#: ../../english/releases/arches.data:20
msgid "Motorola 680x0"
msgstr "Motorola 680x0"

#: ../../english/releases/arches.data:21
msgid "MIPS (big endian)"
msgstr "MIPS (gros-boutiste)"

#: ../../english/releases/arches.data:22
msgid "64-bit MIPS (little endian)"
msgstr "MIPS 64 bits (petit-boutiste)"

#: ../../english/releases/arches.data:23
msgid "MIPS (little endian)"
msgstr "MIPS (petit-boutiste)"

#: ../../english/releases/arches.data:24
msgid "PowerPC"
msgstr "PowerPC"

#: ../../english/releases/arches.data:25
msgid "POWER Processors"
msgstr "PowerPC 64 bits (petit-boutiste)"

#: ../../english/releases/arches.data:26
msgid "IBM S/390"
msgstr "IBM S/390"

#: ../../english/releases/arches.data:27
msgid "IBM System z"
msgstr "System z"

#: ../../english/releases/arches.data:28
msgid "SPARC"
msgstr "SPARC"

#~ msgid "Intel x86"
#~ msgstr "Intel x86"

#~ msgid "AMD64"
#~ msgstr "AMD64"

#~ msgid "HP PA/RISC"
#~ msgstr "HP PA/RISC"

#~ msgid "Intel IA-64"
#~ msgstr "Intel IA-64"

#~ msgid "kFreeBSD (AMD64)"
#~ msgstr "kFreeBSD (AMD64)"

#~ msgid "kFreeBSD (Intel x86)"
#~ msgstr "kFreeBSD (Intel x86)"

#~ msgid "Hurd (i386)"
#~ msgstr "Hurd (i386)"
